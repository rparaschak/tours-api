const restify = require('restify');
const mongoose = require('mongoose');
const corsMiddleware = require('restify-cors-middleware')

const ToursController = require('./api/tours/Tours.controller');
const ActivityController = require('./api/activity/Activity.controller');
const AuthController = require('./api/auth/Auth.controller');

class App {

    constructor(){
        this.server = restify.createServer();
        this.controllers = {};
        this.server.use(restify.plugins.bodyParser());
        this.server.use(restify.plugins.queryParser({ mapParams: false }));
        this.enableCors();
    }

    run(params){
        this.server.listen(params.port, () => {
            console.log('Running on ' + params.port);
        });
    }

    registerController(name, controller){
        this.controllers[name] = new controller(this.server);
    }

    enableCors(){
        const cors = corsMiddleware({
            preflightMaxAge: 5,
            origins: ['http://localhost:4200'],
            allowHeaders: ['API-Token']
        });

        this.server.pre(cors.preflight);
        this.server.use(cors.actual);
    }
}

const app = new App();

app.registerController('ToursController', ToursController);
app.registerController('ActivityController', ActivityController);
app.registerController('AuthController', AuthController);

mongoose.connect('mongodb://localhost/tours');

const db = mongoose.connection;
db.on('error', (err) => {
    console.error('Mongoose: connection error:', err)
});

db.once('open', () => {
    app.run({
        port: 8000
    });
});

process.on('uncaughtException', (err) => {
    console.error(err);
});