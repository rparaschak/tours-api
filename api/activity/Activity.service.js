const Checkin = require('./ActivityCheckin.model');
const Point = require('../tours/Point.model');
const Activity = require('./Activity.model');

const earthRadius = 6378100; /** Meters */

class ActivityService {

    static async finishActivity(activity){
        return await Activity.updateOne({ _id: activity._id}, { active: false });
    }

    static async createCheckin(activity, point){
        return await new Checkin({
            activity: activity._id,
            pointId: point.id,
            createdAt: new Date().toISOString()
        }).save();
    }

    static async getNextPoint(activity){
        let promises = [];
        promises.push(Checkin.find({ activity: activity._id}));
        promises.push(Point.find({ tourId: activity.tourId }));

        const [ checkins, points ] = await Promise.all(promises);

        const lastCheckin = checkins.length && checkins[checkins.length - 1];
        let nextPoint;

        if(!lastCheckin)
            nextPoint = points.find( point => point.order === 0 );
         else {
            const lastPoint = points.find( point => lastCheckin.pointId === point.id );
            nextPoint = points.find( point => lastPoint.order + 1 === point.order );
        }

        if(!nextPoint)
            return false;

         /** marking point if it is the last point in the tour */
         if(nextPoint.order === points.length - 1)
             nextPoint.lastOne = true;

        return nextPoint;
    }

    /**
     * Converts distance in radians into meters
     * @param radians
     */
    static radToKm(radians){
        return radians * earthRadius;
    }

    static getDistanceBetweenPoints(pointA, pointB){
        /** https://stackoverflow.com/questions/27928/calculate-distance-between-two-latitude-longitude-points-haversine-formula */
        const dLat = ActivityService.deg2rad(pointB.lat - pointA.lat);
        const dLon = ActivityService.deg2rad(pointB.lon - pointA.lon);
        const a =
            Math.sin(dLat/2) * Math.sin(dLat/2) +
            Math.cos(ActivityService.deg2rad(pointA.lat)) * Math.cos(ActivityService.deg2rad(pointB.lat)) *
            Math.sin(dLon/2) * Math.sin(dLon/2);
        const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        return earthRadius * c; // Distance in meters
    }

    static deg2rad(deg) {
        return deg * (Math.PI/180)
    }

}

module.exports = ActivityService;