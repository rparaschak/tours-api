const mongoose = require('mongoose');

const ActivitySchema = mongoose.Schema({
    tourId: String,
    userId: mongoose.Schema.Types.ObjectId,
    startedAt: Date,
    active: Boolean,
    checkins: Array
});

/**
 * Queries goes here
 */


ActivitySchema.pre('save', true, (next, done) => {
    next();
    done();
});


const ActivityModel = mongoose.model('Activity', ActivitySchema);
module.exports = ActivityModel;