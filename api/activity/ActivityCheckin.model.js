const mongoose = require('mongoose');

const CheckinSchema = mongoose.Schema({
    activity: String,
    createdAt: Date,
    pointId: String
}, { collection: 'checkins'});

/**
 * Queries go here
 */


/**
 * Hooks go here
 */
CheckinSchema.pre('save', true, (next, done) => {
    next();
    done();
});


const CheckinModel = mongoose.model('ActivityCheckin', CheckinSchema);
module.exports = CheckinModel;