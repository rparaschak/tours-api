const Activity = require('./Activity.model');
const Checkin = require('./ActivityCheckin.model');
const ActivityService = require('./Activity.service');

const userId = "5ad729eccc6c90c4829686db";
const checkinRadius = 500;

class ActivityController {

    constructor(server) {
        server.get('/activity', this.getActivity);
        server.post('/activity', this.createActivity);
        server.post('/activity/checkin', this.checkin);
    }

    async getActivity(req, res, next) {
        try {
            const activity = await Activity.findOne({userId, active: true});
            let checkins;
            if (activity) {
                checkins = await Checkin.find({activity: activity._id});
                activity.checkins = checkins;
            }

            res.status(200);
            res.send(activity || {});
        } catch (err) {
            ActivityController.catchHandler(res, next, err);
        }
    }

    async createActivity(req, res, next) {
        try {
            const activity = await Activity.findOne({userId, active: true});

            if (activity) {
                res.status(409);
                res.send(ActivityController.errors.ACTIVITY_CONFLICT);
                return;
            }

            const newActivity = await new Activity({
                tourId: req.body.tourId,
                userId,
                active: true,
                startedAt: new Date().toISOString()
            }).save();

            res.status(201);
            res.send(newActivity);

        } catch (err) {
            ActivityController.catchHandler(res, next, err);
        }
    }

    async checkin(req, res, next) {
        try {

            if (!req.body.userLocation || !req.body.userLocation.lat || !req.body.userLocation.lon)
                throw ActivityController.errors.LOCATION_REQUIRED;

            const userActivity = await Activity.findOne({userId, active: true});

            if (!userActivity)
                throw ActivityController.errors.CHECKIN_NO_ACTIVITY;

            const nextPoint = await ActivityService.getNextPoint(userActivity);
            if (!nextPoint)
                throw ActivityController.errors.NO_NEXT_CHECKPOINT;

            /** Check if checkpoint is in range */
            const distanceToCheckpoint = ActivityService.getDistanceBetweenPoints(req.body.userLocation, {
                lat: nextPoint.location.coordinates[1],
                lon: nextPoint.location.coordinates[0]
            });

            if (distanceToCheckpoint > checkinRadius)
                throw ActivityController.errors.CHECKIN_NOT_IN_RANGE;

            const nextCheckpoint = await ActivityService.createCheckin(userActivity, nextPoint);

            if (nextPoint.lastOne)
                await ActivityService.finishActivity(userActivity);

            res.status(200);
            res.send(nextCheckpoint);

        } catch (err) {
            ActivityController.catchHandler(res, next, err);
        }
    }

    static catchHandler(res, next, err) {
        console.error(err);
        res.status(err.statusCode || 500);
        res.send(err);
        next();
    }

    static get errors() {
        return {
            ACTIVITY_CONFLICT: {statusCode: 409, code: 2001, message: "There is active activity already."},
            CHECKIN_NO_ACTIVITY: {statusCode: 404, code: 2002, message: "There is no active activity to checkin to."},
            NO_NEXT_CHECKPOINT: { statusCode: 400, code: 2003, message: "There is no next checkpoint. Activity has to be not active anymore."},
            LOCATION_REQUIRED: {statusCode: 400, code: 2004, message: "userLocation parameter is required."},
            CHECKIN_NOT_IN_RANGE: {statusCode: 403, code: 2005, message: "Checkin is not is range."}
        }
    }
}

module.exports = ActivityController;