const Tour = require('./Tour.model');
const Point = require('./Point.model');

class ToursController {

    constructor(server) {
        server.post('/tour', this.createTour);
        server.get('/tour', this.findAll);
        server.get('/tour/:tourId', this.findById);
        server.get('/tour/:tourId/point/:pointId', this.getTourPoint);
        server.get('/tour/:tourId/points', this.getTourPoints);
        server.post('/tour/:tourId/point', this.createPoint);
    }

    async createTour(req, res, next) {
        const tour = new Tour(req.body);

        try {
            await tour.save();
            res.status(201);
            res.send(tour);
            next();
        } catch (e) {
            ToursController.catchHandler(res, next, e);
        }
    };

    async findAll(req, res, next) {
        try {
            const tours = await Tour.find();
            res.send(tours);
            next();
        } catch (e) {
            ToursController.catchHandler(res, next, e);
        }
    };

    async findById(req, res, next) {
        try {
            const tour = await Tour.find().byId(req.params.tourId)
            res.send(tour);
            next();
        } catch (e) {
            ToursController.catchHandler(res, next, e);
        }
    };

    async getTourPoint(req, res, next) {
    };

    async getTourPoints(req, res, next) {
        try {
            const points = await Point.find({tourId: req.params.tourId}).sort({order: 1});
            res.send(points);
            next();
        } catch (e) {
            ToursController.catchHandler(res, next, e);
        }


    };

    createPoint() {
    }

    static catchHandler(res, next, err) {
        console.error(err);
        res.status(err.statusCode || 500);
        res.send(err);
        next();
    }

    static get errors() {
        return {
            ACTIVITY_CONFLICT: {statusCode: 409, code: 2001, message: "There is active activity already."},
            CHECKIN_NO_ACTIVITY: {statusCode: 404, code: 2002, message: "There is no active activity to checkin to."},
            NO_NEXT_CHECKPOINT: {
                statusCode: 400,
                code: 2003,
                message: "There is no next checkpoint. Activity has to be not active anymore."
            }
        };
    }
}

module.exports = ToursController;