const mongoose = require('mongoose');

const TourSchema = mongoose.Schema({
    id: String, /** Friendly id to display in url */
    name: String,
    description: String,
    coverImageUrl: String,
    images: [{url: String}],
    meta: mongoose.Schema.Types.Mixed,
    points: [{ type: String, ref: 'TourPoint' }]
});

/**
 * Queries goes here
 */

TourSchema.query.byId = (id) => {
    return TourModel.findOne({ id });
};

TourSchema.pre('save', true, (next, done) => {
    next();
    done();
});


const TourModel = mongoose.model('Tour', TourSchema);
module.exports = TourModel;