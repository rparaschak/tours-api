const mongoose = require('mongoose');

const PointSchema = mongoose.Schema({
    id: String, /** Friendly id to display in url */
    tourId: String,
    name: String,
    description: String,
    coverImageUrl: String,
    images: [{url: String}],
    order: Number,
    location: mongoose.Schema.Types.Mixed
}, { collection: "points"});

/**
 * Queries goes here
 */

PointSchema.pre('save', true, (next, done) => {
    next();
    done();
});


const TourPointModel = mongoose.model('TourPoint', PointSchema);
module.exports = TourPointModel;