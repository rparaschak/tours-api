const AuthService = require('./AuthService');
const User = require('../users/User.model');

class AuthController {

    constructor(server) {
        server.post('/login/facebook', this.facebookLogin);
    }

    async facebookLogin({ body: { accessToken, signedRequest } }, res, next) {
        try {
            if(!accessToken || !signedRequest)
                throw AuthController.errors.NO_FACEBOOK_AUTH;

            const payload = AuthService.decodeFacebookSignedRequest(signedRequest);

            if(!payload)
                throw AuthService.errors.WRONG_FACEBOOK_SIGNED_REQUEST;

            let user = await User.findOne({ 'facebook.userID': payload.user_id  });

            if(!user)
                user = await AuthService

            res.status(201);
            res.send({ a:1 });
            next();
        } catch (e) {
            AuthController.catchHandler(res, next, e);
        }
    };



    static catchHandler(res, next, err) {
        console.error(err);
        res.status(err.statusCode || 500);
        res.send(err);
        next();
    }

    static get errors() {
        return {
            NO_FACEBOOK_AUTH: {statusCode: 400, code: 3001, message: "accessToken and signedRequest parameter is required."},
            WRONG_FACEBOOK_SIGNED_REQUEST: { statusCode: 403, code: 3002, message: "Wrong facebook signed request."},
        };
    }
}

module.exports = AuthController;