const mongoose = require('mongoose');

const AuthTokenSchema = mongoose.Schema({
    userId: mongoose.Schema.Types.ObjectId,
    facebook: mongoose.Schema.Types.Mixed
}, { collection: "auth_tokens"});

/**
 * Queries goes here
 */

const AuthTokenModel = mongoose.model('AuthToken', AuthTokenSchema);
module.exports = AuthTokenModel;