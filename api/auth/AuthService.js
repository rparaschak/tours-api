const crypto = require('crypto');

class AuthService {

    static get facebookAppSecret() {
        return '0183d16a548abf2e85485d90861c8bdc';
    }

    static decodeFacebookSignedRequest(signedRequest) {
        let [encodedSignature, encodedPayload] = signedRequest.split('.', 2);

        const payload = JSON.parse(Buffer.from(encodedPayload, 'base64'));

        if (!payload.algorithm || payload.algorithm.toUpperCase() !== 'HMAC-SHA256')
            throw new Error("Unknown algorithm. Expected HMAC-SHA256");

        const expectedSignature = crypto
            .createHmac('sha256', AuthService.facebookAppSecret)
            .update(encodedPayload)
            .digest('base64')
            .replace(/\+/g, '-')
            .replace(/\//g, '_')
            .replace('=', '');

        if(encodedSignature === expectedSignature)
            return payload;
    }

}

module.exports = AuthService;