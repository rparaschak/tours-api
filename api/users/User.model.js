const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
    fullName: String,
    email: String,
    avatarUrl: mongoose.Schema.Types.Mixed,
    facebook: mongoose.Schema.Types.Mixed
}, { collection: "users"});

/**
 * Queries goes here
 */

const UserModel = mongoose.model('User', UserSchema);
module.exports = UserModel;