const User = require('./User.model');

class UserService {

    static async createUser({ email, facebook }){
        const user = new User({ email, facebook });
        return user.save();
    }

}